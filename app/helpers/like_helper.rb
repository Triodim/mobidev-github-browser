module LikeHelper

  def get_item_status (item_id, item_data)
    if Like.where("#{item_data} = #{item_id}").first.present?
      status = Like.find_by("#{item_data} = #{item_id}").status
    else
      status = false
    end
    status
  end
  
end
