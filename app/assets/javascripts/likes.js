$(document).on('turbolinks:load', function() {

  $('.repo-contributors').css("display","flex");

  $('.like-button').on('click', function(){

    var status = $(this).text().indexOf('U') > -1;
    var itemId = $(this).attr('data-item-id');
    var item = $(this).attr('data-item');

    $.ajax({
      url: '/change_like_status',
      data: {id: itemId, item: item},
      type: "PUT",
      success: function(){
        if (status) {
          $("[data-item-id=" + itemId + "]").html('Like');
        } else {
          $("[data-item-id=" + itemId + "]").html('Unlike');
        };
      },
      error: function(error){
        console.log("Error");
      }
    });
  });
});
