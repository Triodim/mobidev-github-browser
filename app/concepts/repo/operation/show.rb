class Repo::Show < Trailblazer::Operation

  step :initialize_client
  step :repo_info
  step :repo_contributors

  def initialize_client(options, **)
    options[:client] = Octokit::Client.new(:login => 'Triodim', :password => 'Dimmon1980')
  end

  def repo_info(options, params:, **)
    options[:item] = options[:client].repository(params.to_i)
  end

  def repo_contributors(options, params:, **)

    options[:conributors] = options[:client].contributors(params.to_i)

  end

end
