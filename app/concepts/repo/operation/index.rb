class Repo::Index < Trailblazer::Operation

  step :initialize_client
  step :show_all

  def initialize_client(options, **)
    options[:client] = Octokit::Client.new(:login => ENV["GITHUB_LOGIN"], :password => ENV["GITHUB_PASS"])
  end

  def show_all(options, params:, **)
    options[:items] = options[:client].search_repositories(params, options = {sort: "stars", order: "desc"}).items
  end

end
