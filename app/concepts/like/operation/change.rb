class Like::Change < Trailblazer::Operation

  pass :find_status
  step :set_status

  def find_status(options, params:, **)
    options[:current_status] = Like.where("#{params[:item]}=#{params[:id]}").first_or_create(status: false, "#{params[:item]}": "#{params[:id]}").status
  end

  def set_status(options, params:, **)
      set_item_status(options[:current_status], params[:id], params[:item])
  end

  def set_item_status(current_status, item_id, item_data)
    if current_status
      Like.find_by("#{item_data} = #{item_id}").update(status: false)
    else
      Like.find_by("#{item_data} = #{item_id}").update(status: true)
    end
  end

end
