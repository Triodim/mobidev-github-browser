class ReposController < ApplicationController

  def index
    if params[:search_query].length > 0
      result = Repo::Index.(params: params[:search_query])
    else
      params[:search_query] = "trailblazer"
      result = Repo::Index.(params: params[:search_query])
    end
    @repos = result[:items]
    add_breadcrumb "For search term \"#{params[:search_query]}\" found", repos_path
  end

  def show
    if params[:id].present?
      result = Repo::Show.(params: params[:id])
      add_breadcrumb "#{result[:item].full_name}"
    else
      params[:id] = "10955751"
      result = Repo::Show.(params: params[:id])
    end
    @repo = result[:item]
    @contributors = result[:conributors]
  end

end
