class ApplicationController < ActionController::Base

  add_breadcrumb "Main", :root_path

  def action_missing (action)
    redirect_to root_path if action != 'landing'
  end
  
end
