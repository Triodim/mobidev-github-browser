class LikesController < ApplicationController
protect_from_forgery :except => [:change_like_status]

  def change_like_status
      result = Like::Change.(params: params)
      if result.success?
        render json: {}, status: :ok
      else
        render json: {}, status: 404
      end
  end

end
