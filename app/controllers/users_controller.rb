class UsersController < ApplicationController

  def show
    if params[:id]
      result = User::Show.(params: params[:id])
      @user = result[:user]
      add_breadcrumb "User #{@user.login}"
    else
      @messege = "No such User on GitHub"
      params[:search_query] = "ruby"
    end
  end

end
