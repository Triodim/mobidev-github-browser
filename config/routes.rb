Rails.application.routes.draw do

  resources :repos
  resources :users
  resources :likes

  put '/change_like_status', to: 'likes#change_like_status'

  root to: 'repos#show'
end
