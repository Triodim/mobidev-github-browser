class CreateLikes < ActiveRecord::Migration[5.2]
  def change
    create_table :likes do |t|
      t.boolean :status
      t.integer :repo_id
      t.integer :user_id

      t.timestamps
    end
    add_index :likes, :repo_id, unique: true
    add_index :likes, :user_id, unique: true
  end
end
