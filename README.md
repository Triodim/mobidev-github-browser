# README

To quick start this application you should:

- clone this app to your disk
- set two constants in .env, to reach GitHubAPI
  ENV["GITHUB_PASS"]
  ENV["GITHUB_LOGIN"]
- install docker and run docker
- from console run commands:
- 1 - "docker-compose run web bundle install"
- 2 - "docker-compose build"
- 3 - "docker-compose run web rake db:create"
- 4 - "docker-compose up"
- the application will be available on the page http://localhost:3000
